import Vue from 'vue'
import Router from 'vue-router'
import Body from '@/demo/Body'
import Foo from '@/demo/Foo'
import Form from '@/demo/form'
import Table from '@/demo/table'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'body',
      component: Body
    },
    {
      path: '/form',
      name: 'form',
      component: Form
    },
    {
      path: '/foo',
      name: 'foo',
      component: Foo
    },
    {
      path: '/modules/table',
      name: 'table',
      component: Table
    }
  ]
})
